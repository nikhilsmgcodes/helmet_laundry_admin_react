import React, { Component } from "react";
import {Table } from "react-bootstrap";
import Card from "components/Card/Card";
import Select from 'react-select';

class Users extends Component {

  constructor(props){
    super(props);

    this.state = {
      usersList : [],
      selectedOption: []
    };
    if(!sessionStorage.getItem("key") || !sessionStorage.getItem("user")){
      window.location.hash = '/login';
    }
  }

  componentDidMount(){
    this.getUsersList();
  }

  handleSelectChange = (selectedOption) => {
    this.setState({ selectedOption });
  }

  //Ajax Calls Start

  getUsersList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getUsersList',{
      method: 'GET',
      headers: {
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
      this.setState({
        usersList :json.data
      });
    }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  //Ajax Calls End
  //Views Start

  tableData(){
    return (
      <Table responsive striped bordered condensed hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Address</th>
            </tr>
          </thead>
          <tbody>
            {this.state.usersList.map(item => {
              if(this.state.selectedOption.length <= 0 || item.user_id === this.state.selectedOption.value ){
                  return (
                    <tr key={item.user_id}>
                      <td>{item.user_id}</td>
                      <td>{item.name}</td>
                      <td>{item.phone}</td>
                      <td>{item.email}</td>
                      <td>{item.address}</td>
                    </tr>
                )
              }
              else{
                return null;
              }
            })}
          </tbody>
        </Table>
    )
  }
  
  render() {
    const selectList = this.state.usersList.map(json =>{
        return { value: json.user_id , label: json.name}
    })
    return (
      <div className="content">
          <Card
                // title="BRANDS"
                // category="List Of Brands"
                content={
                  <div>
                    <div className='row'>
                      <div className='col-sm-4'>
                      <Select placeholder='Search By Name..'
                        value={this.state.selectedOption}
                        onChange={this.handleSelectChange}
                        options={selectList}
                      />
                      </div>
                    </div>
                    <br />
                    {this.tableData()}
                  </div>
                }
          />
      </div>
    );
  }
}

export default Users;

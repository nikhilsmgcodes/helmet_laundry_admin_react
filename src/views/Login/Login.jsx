import React, { Component } from "react";

import backImg from "assets/img/back1.jpg";
import helmetCam from "assets/img/helmetCam.png";

class Login extends Component {
    constructor(props){
        super(props);
    
        this.state = {
            user_id : '',
            password : '',
        };
    }

    componentDidMount() {
        // this.checkSession();
        document.body.style.backgroundImage = 'url(' + backImg + ')';
        document.body.style.backgroundSize= 'cover';
        document.body.style.minHeight= 'unset';
        document.body.style.backgroundPosition = 'center';
    }

    componentWillUnmount(){
        document.body.style.backgroundImage = '';
    }

    handelUserNameChange(event){
        this.setState({
            user_id: event.target.value
          })
    }

    handelPassChange(event){
        this.setState({
            password: event.target.value
        })
    }


    checkSession(){
        fetch('http://softbizz.in/helmetLaundry/api/public/checkSessionData',{
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'session-key': sessionStorage.getItem("key"),
            'user-id' : sessionStorage.getItem("user")
          }
        })
        .then(response => response.json())
        .then(json =>{
          if(json.error === true){
            if(json.sessionExpired === true){
              this.setState({
                showLogin : true,
              })
            }
          }
          else{
            window.location.hash = 'dashboard';
            this.setState({
              showLogin : false},
            //   () => window.location.reload()
          );
         }
        })
        .catch(error =>{
          console.log(error);
        });
      }

    adminLogin(){
        fetch('http://softbizz.in/helmetLaundry/api/public/adminLogin',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'user_id': this.state.user_id,
                'password': this.state.password,
            })
            })
            .then(response => response.json())
            .then(json =>{
                if(json.error === true){
                    alert(json.message);
                }
                else{
                    window.sessionStorage.setItem('user','admin');
                    window.sessionStorage.setItem('key',json.data);
                    window.location.hash = '/dashboard';
                }
            })
            .catch(error =>{
                console.log(error);
            });
    }

    render() {
        var loginForm = {
            width: '340px',
    	    margin: '50px auto'
        }
        var formIn = {
            marginBottom: '15px',
            background: 'rgb(178, 185, 181)',
            boxShadow: '4px 4px 4px rgba(2, 2, 2, 0.3)',
            padding: '30px'
        }
        var headCol = {
            color: 'white'
        }
        var btnColor = {
            backgroundColor: '#0076bd',
            borderColor: '#002885'
        }
        var btnTxt ={
            color: 'white'
        }
        return(
            <div className="content">
           
                    <div>
                        <h2 className="text-center" style={headCol}><b>HELMET  LAUNDRY</b></h2>
                        <div style={loginForm}>
                            <form style={formIn}>
                            <div className="text-center">
                                    <div className="logo-img">
                                    <img src={helmetCam} alt="logo_image" />
                                    </div>
                            </div>   
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Username" required="required" onChange={this.handelUserNameChange.bind(this)}/>
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-control" placeholder="Password" required="required" onChange={this.handelPassChange.bind(this)} />
                                </div>
                                <div className="form-group">
                                    <button type="submit" style={btnColor} className="btn btn-primary btn-block" onClick={this.adminLogin.bind(this)}><b style={btnTxt}>LOG IN</b></button>
                                </div>      
                            </form>
                        </div>
                    </div>
      
            </div>
        )
    }
}

export default Login;
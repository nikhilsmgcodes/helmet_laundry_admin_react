import React, { Component } from "react";
import {Table, Image, ButtonToolbar, Button, Modal } from "react-bootstrap";
import Card from "components/Card/Card";
import Select from 'react-select';

class Category extends Component {

  constructor(props){
    super(props);

    this.state = {
      categoryList : [],
      show: false,
      editId : '',
      showAddModel: false,
      categoryName: '',
      image: {file: '' ,imagePreviewUrl: ''},
      imageSelected : false,
      tempData: {},
      selectedOption: []
    };
    if(!sessionStorage.getItem("key") || !sessionStorage.getItem("user")){
      window.location.hash = '/login';
    }
  }

  componentDidMount(){
    this.getCategoryList();
  }

  handleClose() {
    this.setState({
       show: false, 
       showAddModel: false,
       categoryName:'', 
       image: {file: '' ,imagePreviewUrl: ''},
       imageSelected : false,
       categoryNameChanged: false,
       tempData: {}
      });
  }

  handleShowAddModel(){
    this.setState({ showAddModel: true });
  }


  handleShowEditModel(item) {
    this.setState({ 
      show: true,
      tempData: item,
      image:{imagePreviewUrl: item.category_image},
      categoryName: item.category_name
    });
  }

  handleCategoryNameChange(event) {
    this.setState({
      categoryNameChanged: true,
      categoryName: event.target.value
    });
  }

  handleImageChange(event) {
    this.setState({imageSelected : true});
    event.preventDefault();

    let reader = new FileReader();
    let file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({
        image:{
          file: file,
          imagePreviewUrl: reader.result
        }
      });
    }
    reader.readAsDataURL(file);
    console.log(file);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
  }

  //Ajax Call Start

  getCategoryList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getCategoryList',{
      method: 'GET',
      headers: {
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
      this.setState({
        categoryList :json.data
      });
    }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  handleToggle(item){
    var con;
    if(item.flag === 1){
      con = window.confirm('Want to DeActivate Category?');
    }
    if(item.flag === 0){
      con = window.confirm('Want to Activate Category?');
    }
    if(con){
      fetch('http://softbizz.in/helmetLaundry/api/public/toggleCategory',{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'session-key': sessionStorage.getItem("key"),
          'user-id' : sessionStorage.getItem("user")
        },
        body: JSON.stringify({
          'category_id': item.category_id,
        })
      })
      .then(response => response.json())
      .then(json =>{
        if(json.error === true){
          if(json.sessionExpired === true){
            window.location.hash = 'login';
          }
          alert(json.message);
        }
        else{
          alert(json.message);
          this.handleClose();
          this.getCategoryList();
        }
      })
      .catch(error =>{
        console.log(error);
      });
    }
  }

  updateCategory(){
    if(this.state.categoryName === ''){
      alert('Please Enter Category Name');
      return;
    }
    if(this.state.image.imagePreviewUrl === ''){
      alert('Please Select Category Image');
      return;
    }
    var categoryName = this.state.categoryName;
    var image = this.state.image.imagePreviewUrl;
    var tempCategoryName = this.state.tempData.category_name;
    var tempImage = this.state.tempData.category_image;
    if(categoryName === tempCategoryName && image === tempImage){
      alert('No Changes to Update');
      return;
    }
    var imagePreviewUrl;
    if(this.state.imageSelected === false){
      imagePreviewUrl = '';
    }
    else{
      imagePreviewUrl = this.state.image.imagePreviewUrl;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/updateCategory',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
      body: JSON.stringify({
        'category_id': this.state.tempData.category_id,
        'category_name': this.state.categoryName,
        'category_image': imagePreviewUrl
      })
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        alert(json.message);
        this.handleClose();
        this.getCategoryList();
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  addNewCategory(){
    if(this.state.categoryName === ''){
      alert('Please Enter Category Name');
      return;
    }
    if(this.state.image.imagePreviewUrl === ''){
      alert('Please Select Category Image');
      return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/addCategory',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
      body: JSON.stringify({
        'category_name': this.state.categoryName,
        'category_image': this.state.image.imagePreviewUrl
      })
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        alert(json.message);
        this.handleClose();
        this.getCategoryList();
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  //Ajax Call End
  //Views Start

  editModel(){
    let {imagePreviewUrl} = this.state.image;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<Image src={imagePreviewUrl} thumbnail width='200px' height='200px'/>);
    } else {
      $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    }
    
    return(
      <Modal show={this.state.show} onHide={this.handleClose.bind(this)}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Category</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container-fluid ">
          <div className='row'>
            <div className='col-sm-4'>
              <label>Category Name</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' value={this.state.categoryName} onChange={this.handleCategoryNameChange.bind(this)} placeholder='Enter Category Name'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Category Image</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type="file" onChange={(event)=>this.handleImageChange(event)} />     
              <div className="imgPreview">
                {$imagePreview}
              </div>
            </div>          
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
            </div> 
            <div className='col-sm-8'>
              <Button bsStyle='success' onClick={this.updateCategory.bind(this)}>Update</Button>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.handleClose.bind(this)}>Close</Button>
      </Modal.Footer>
    </Modal>
    )
  }

  addModel(){
    let {imagePreviewUrl} = this.state.image;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<Image src={imagePreviewUrl} thumbnail width='200px' height='200px'/>);
    } else {
      $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    }
    
    return(
      <Modal show={this.state.showAddModel} onHide={this.handleClose.bind(this)}>
      <Modal.Header closeButton>
        <Modal.Title>Add New Category</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container-fluid ">
          <div className='row'>
            <div className='col-sm-4'>
              <label>Category Name</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' value={this.state.categoryName} onChange={this.handleCategoryNameChange.bind(this)} placeholder='Enter Category Name'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Category Image</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type="file" onChange={(event)=>this.handleImageChange(event)} />     
              <div className="imgPreview">
                {$imagePreview}
              </div>
            </div>          
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
            </div> 
            <div className='col-sm-8'>
              <Button bsStyle='success' onClick={this.addNewCategory.bind(this)}>ADD</Button>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.handleClose.bind(this)}>Close</Button>
      </Modal.Footer>
    </Modal>
    )
  }

  tableData(){
    return (
      <Table responsive striped bordered condensed hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>Category Name</th>
              <th>Category Image</th>
              <th>Options</th>
            </tr>
          </thead>
          <tbody>
            {this.state.categoryList.map(item => {
              if(this.state.selectedOption.length <= 0 || item.category_id === this.state.selectedOption.value ){
                  return (
                    <tr key={item.category_id}>
                      <td>{item.category_id}</td>
                      <td>{item.category_name}</td>
                      <td><Image src={item.category_image} thumbnail width='100px' height='100px'/></td>
                      <td>
                        <ButtonToolbar>
                          <Button bsStyle="primary" bsSize="small" onClick={this.handleShowEditModel.bind(this, item)}>Edit</Button>
                          {item.flag === 1 ? 
                                 (<Button bsStyle="danger" bsSize="small" onClick={this.handleToggle.bind(this, item)}>Deactivate</Button>) 
                                :(<Button bsStyle="success" bsSize="small" onClick={this.handleToggle.bind(this, item)}>Activate</Button>)
                          }
                        </ButtonToolbar>
                      </td>
                    </tr>
                )
              }
              else{
                return null;
              }
            })}
          </tbody>
        </Table>
    )
  }
  
  render() {
    const selectList = this.state.categoryList.map(json =>{
        return { value: json.category_id , label: json.category_name}
    })
    return (
      <div className="content">
          <Card
                // title="BRANDS"
                // category="List Of Categorys"
                content={
                  <div>
                    <div className='row'>
                      <div className='col-sm-6'>
                        <Button bsStyle="success" onClick={this.handleShowAddModel.bind(this)}>ADD NEW CATEGORY</Button>
                      </div>
                      <div className='col-sm-6'>
                      <Select placeholder='Search By Category Name...'
                        value={this.state.selectedOption}
                        onChange={this.handleChange}
                        options={selectList}
                      />
                      </div>
                    </div>
                    <br />
                    {this.tableData()}
                    {this.editModel()}
                    {this.addModel()}
                  </div>
                }
          />
      </div>
    );
  }
}

export default Category;

import React, { Component } from "react";
import {Table, Image, ButtonToolbar, Button, Modal } from "react-bootstrap";
import Card from "components/Card/Card";
import Select from 'react-select';

class Services extends Component {

  constructor(props){
    super(props);

    this.state = {
      categoryList : [],
      show: false,
      editId : '',
      showAddModel: false,
      categoryName: '',
      image: {file: '' ,imagePreviewUrl: ''},
      imageSelected : false,
      tempData: {service_id:'', category_id: '',service_name:'', service_image:'', service_description:'', price:'', sla:'', reminder_days:''},
      tempDataOriginal: {},
      selectedOption: [],
      servicesList : [],
      selectedCategory : {},
      selectedCategoryTemp: {}
    };
    if(!sessionStorage.getItem("key") || !sessionStorage.getItem("user")){
      window.location.hash = '/login';
    }
  }

  componentDidMount(){
    this.getCategoryList();
    this.getServicesList();
  }

  handleClose() {
    this.setState({
       show: false,
       showAddModel: false,
       image: {file: '' ,imagePreviewUrl: ''},
       imageSelected : false,
       tempData: {service_id:'', category_id: '',service_name:'', service_image:'', service_description:'', price:'', sla:'', reminder_days:''},
       tempDataOriginal: {},
       selectedCategory: '',
       selectedCategoryTemp: ''
      });
  }

  handleShowAddModel(){
    this.setState({ showAddModel: true });
  }

  handleShowEditModel(item) {
    this.setState({
      show: true,
      tempData: item,
      tempDataOriginal: item,
      image:{imagePreviewUrl: item.service_image},
    });

    const categoryNameList = this.state.categoryList.map(json =>{
      return { value: json.category_id , label: json.category_name}
    });

    for(var i=0; i<categoryNameList.length; i++){
      if(categoryNameList[i].value === item.category_id){
        this.setState({
          selectedCategory: categoryNameList[i],
          selectedCategoryTemp: categoryNameList[i]
        });
      }
    }
  }

  handleCategorySelectedChange = (selectedCategory) => {
    this.setState({ selectedCategory });
  }

  handleServiceNameChange(event) {
    this.setState({
      tempData:{...this.state.tempData, service_name: event.target.value}
    });
  }

  handleServicePriceChange(event) {
    this.setState({
      tempData:{...this.state.tempData, price: event.target.value}
    });
  }

  handleSLAChange(event) {
    this.setState({
      tempData:{...this.state.tempData, sla: event.target.value}
    });
  }

  handleReminderDaysChange(event) {
    this.setState({
      tempData:{...this.state.tempData, reminder_days: event.target.value}
    });
  }

  handleImageChange(event) {
    this.setState({imageSelected : true});
    event.preventDefault();

    let reader = new FileReader();
    let file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({
        image:{
          file: file,
          imagePreviewUrl: reader.result
        }
      });
    }
    reader.readAsDataURL(file);
    console.log(file);
  }

  handleServiceDescriptionChange(event){
    this.setState({
      tempData:{...this.state.tempData, service_description: event.target.value}
    });
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
  }

  //Ajax Call Start

  getServicesList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getServicesList',{
      method: 'GET',
      headers: {
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
      this.setState({
        servicesList :json.data
      });
    }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  getCategoryList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getCategoryList',{
      method: 'GET',
      headers: {
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
      this.setState({
        categoryList :json.data
      });
    }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  handleToggle(item){
    var con;
    if(item.flag === 1){
      con = window.confirm('Want to DeActivate Service?');
    }
    if(item.flag === 0){
      con = window.confirm('Want to Activate Service?');
    }
    if(con){
      fetch('http://softbizz.in/helmetLaundry/api/public/toggleService',{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'session-key': sessionStorage.getItem("key"),
          'user-id' : sessionStorage.getItem("user")
        },
        body: JSON.stringify({
          'service_id': item.service_id,
        })
      })
      .then(response => response.json())
      .then(json =>{
        if(json.error === true){
          if(json.sessionExpired === true){
            window.location.hash = 'login';
          }
          alert(json.message);
        }
        else{
          alert(json.message);
          this.handleClose();
          this.getServicesList();
        }
      })
      .catch(error =>{
        console.log(error);
      });
    }
  }

  updateService(){
    var t = this.state.tempData;
    var t1 = this.state.tempDataOriginal;
    if(t.service_name === '' || t.price === '' || t.sla === '' || t.reminder_days === '' || t.service_description === '' || this.state.selectedCategory.length <= 0){
      alert('Please re-Check All Fields');
      return;
    }
    if(this.state.image.imagePreviewUrl === ''){
      alert('Please Select Service Image');
      return;
    }

    if(t === t1 && this.state.imageSelected === false && this.state.selectedCategory === this.state.selectedCategoryTemp){
      alert('No Changes to Update');
      return;
    }

    var imagePreviewUrl;
    if(this.state.imageSelected === false){
      imagePreviewUrl = '';
    }
    else{
      imagePreviewUrl = this.state.image.imagePreviewUrl;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/updateService',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
      body: JSON.stringify({
        'service_id' : this.state.tempData.service_id,
        'category_id' : this.state.selectedCategory.value,
        'service_name' : this.state.tempData.service_name,
        'service_image' : imagePreviewUrl,
        'service_description' : this.state.tempData.service_description,
        'price' : this.state.tempData.price,
        'sla' : this.state.tempData.sla,
        'reminder_days' : this.state.tempData.reminder_days,
      })
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        alert(json.message);
        this.handleClose();
        this.getServicesList();
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  addNewService(){
    var t = this.state.tempData;
    if(t.service_name === '' || t.price === '' || t.sla === '' || t.reminder_days === '' || t.service_description === '' || this.state.selectedCategory.length <= 0){
      alert('Please fill All Fields');
      return;
    }
    
    if(this.state.image.imagePreviewUrl === ''){
      alert('Please Select Service Image');
      return;
    }

    fetch('http://softbizz.in/helmetLaundry/api/public/addService',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
      body: JSON.stringify({
        'service_id' : this.state.tempData.service_id,
        'category_id' : this.state.selectedCategory.value,
        'service_name' : this.state.tempData.service_name,
        'service_image' : this.state.image.imagePreviewUrl,
        'service_description' : this.state.tempData.service_description,
        'price' : this.state.tempData.price,
        'sla' : this.state.tempData.sla,
        'reminder_days' : this.state.tempData.reminder_days,
      })
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        alert(json.message);
        this.handleClose();
        this.getServicesList();
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  //Ajax Call End
  //Views Start

  addModel(){
    let {imagePreviewUrl} = this.state.image;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<Image src={imagePreviewUrl} thumbnail width='200px' height='200px'/>);
    } else {
      $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    }

    const categoryNameList = this.state.categoryList.map(json =>{
      return { value: json.category_id , label: json.category_name}
    });

    return(
      <Modal show={this.state.showAddModel} onHide={this.handleClose.bind(this)}>
      <Modal.Header closeButton>
        <Modal.Title>Add New Service</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container-fluid ">
          <div className='row'>
            <div className='col-sm-4'>
              <label>Service Name</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' onChange={this.handleServiceNameChange.bind(this)} placeholder='Enter Service Name' />
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Category</label>
            </div>
            <div className='col-sm-8'>
            <Select placeholder='Select Category'
                  onChange={this.handleCategorySelectedChange}
                  options={categoryNameList}
                  />
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Service Image</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type="file" onChange={(event)=>this.handleImageChange(event)} />
              <div className="imgPreview">
                {$imagePreview}
              </div>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Service Description</label>
            </div>
            <div className='col-sm-8'>
              <textarea className="form-control" type='text' onChange={this.handleServiceDescriptionChange.bind(this)} placeholder='Enter Service Description'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Service Price</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' onChange={this.handleServicePriceChange.bind(this)} placeholder='Enter Service Price'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>SLA</label>
            </div>
            <div className='col-sm-8'>
              <textarea className="form-control" type='text' onChange={this.handleSLAChange.bind(this)} placeholder='Enter SLA'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Reminder Days</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' onChange={this.handleReminderDaysChange.bind(this)} placeholder='Enter Reminder Days'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
            </div>
            <div className='col-sm-8'>
              <Button bsStyle='success' onClick={this.addNewService.bind(this)}>ADD</Button>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.handleClose.bind(this)}>Close</Button>
      </Modal.Footer>
    </Modal>
    )
  }

  editModel(){
    let {imagePreviewUrl} = this.state.image;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<Image src={imagePreviewUrl} thumbnail width='200px' height='200px'/>);
    } else {
      $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    }

    const categoryNameList = this.state.categoryList.map(json =>{
      return { value: json.category_id , label: json.category_name}
    });

    return(
      <Modal show={this.state.show} onHide={this.handleClose.bind(this)}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Service</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container-fluid ">
          <div className='row'>
            <div className='col-sm-4'>
              <label>Service Name</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' value={this.state.tempData.service_name} onChange={this.handleServiceNameChange.bind(this)} placeholder='Enter Service Name' />
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Category</label>
            </div>
            <div className='col-sm-8'>
            <Select placeholder='Select Category'
                  value={this.state.selectedCategory}
                  onChange={this.handleCategorySelectedChange}
                  options={categoryNameList}
                  />
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Service Image</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type="file" onChange={(event)=>this.handleImageChange(event)} />
              <div className="imgPreview">
                {$imagePreview}
              </div>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Service Description</label>
            </div>
            <div className='col-sm-8'>
              <textarea className="form-control" type='text' value={this.state.tempData.service_description} onChange={this.handleServiceDescriptionChange.bind(this)} placeholder='Enter Service Description'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Service Price</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' value={this.state.tempData.price} onChange={this.handleServicePriceChange.bind(this)} placeholder='Enter Service Price'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>SLA</label>
            </div>
            <div className='col-sm-8'>
              <textarea className="form-control" type='text' value={this.state.tempData.sla} onChange={this.handleSLAChange.bind(this)} placeholder='Enter SLA'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Reminder Days</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' value={this.state.tempData.reminder_days} onChange={this.handleReminderDaysChange.bind(this)} placeholder='Enter Reminder Days'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
            </div>
            <div className='col-sm-8'>
              <Button bsStyle='success' onClick={this.updateService.bind(this)}>Update</Button>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.handleClose.bind(this)}>Close</Button>
      </Modal.Footer>
    </Modal>
    )
  }

  tableData(){
    return (
      <Table responsive striped bordered condensed hover>
          <thead>
            <tr>
              <th>Service ID</th>
              <th>Category</th>
              <th>Service Name</th>
              <th>Service Image</th>
              <th>Service Description</th>
              <th>Price</th>
              <th>SLA</th>
              <th>Reminder Days</th>
              <th>Options</th>
            </tr>
          </thead>
          <tbody>
            {this.state.servicesList.map(item => {
              if(this.state.selectedOption.length <= 0 || item.category_id === this.state.selectedOption.value ){
                  return (
                    <tr key={item.service_id}>
                      <td>{item.service_id}</td>
                      <td>{item.category_name}</td>
                      <td>{item.service_name}</td>
                      <td><Image src={item.service_image} thumbnail width='100px' height='100px'/></td>
                      <td>{item.service_description}</td>
                      <td>{item.price}</td>
                      <td>{item.sla}</td>
                      <td>{item.reminder_days}</td>
                      <td>
                        <ButtonToolbar>
                          <Button bsStyle="primary" bsSize="small" onClick={this.handleShowEditModel.bind(this, item)}>Edit</Button>
                          {item.flag === 1 ? 
                                 (<Button bsStyle="danger" bsSize="small" onClick={this.handleToggle.bind(this, item)}>Deactivate</Button>) 
                                :(<Button bsStyle="success" bsSize="small" onClick={this.handleToggle.bind(this, item)}>Activate</Button>)
                          }
                        </ButtonToolbar>
                      </td>
                    </tr>
                )
              }
              else{
                return null;
              }
            })}
          </tbody>
        </Table>
    )
  }

  render() {
    const selectList = this.state.categoryList.map(json =>{
        return { value: json.category_id , label: (json.category_id +" "+ json.category_name)}
    });
    return (
      <div className="content">
          <Card
                // title="BRANDS"
                // category="List Of Categorys"
                content={
                  <div>
                    <div className='row'>
                      <div className='col-sm-6'>
                        <Button bsStyle="success" onClick={this.handleShowAddModel.bind(this)}>ADD NEW SERVICE</Button>
                      </div>
                      <div className='col-sm-6'>
                      <Select placeholder='Search By Category...'
                        value={this.state.selectedOption}
                        onChange={this.handleChange}
                        options={selectList}
                      />
                      </div>
                    </div>
                    <br />
                    {this.tableData()}
                    {this.editModel()}
                    {this.addModel()}
                  </div>
                }
          />
      </div>
    );
  }
}

export default Services;

import React, { Component } from "react";
import {Table, Image, ButtonToolbar, Button, Modal } from "react-bootstrap";
import Card from "components/Card/Card";
import Select from 'react-select';

class Brands extends Component {

  constructor(props){
    super(props);

    this.state = {
      brandList : [],
      show: false,
      editId : '',
      showAddModel: false,
      brandName: '',
      image: {file: '' ,imagePreviewUrl: ''},
      imageSelected : false,
      tempData: {},
      selectedOption: []
    };
    if(!sessionStorage.getItem("key") || !sessionStorage.getItem("user")){
      window.location.hash = '/login';
    }
  }

  componentDidMount(){
    this.getBrandList();
  }

  //Start

  handleClose() {
    this.setState({
       show: false, 
       showAddModel: false,
       brandName:'', 
       image: {file: '' ,imagePreviewUrl: ''},
       imageSelected : false,
       brandNameChanged: false,
       tempData: {}
      });
  }

  handleShowAddModel(){
    this.setState({ showAddModel: true });
  }

  handleShowEditModel(item) {
    this.setState({ 
      show: true,
      tempData: item,
      image:{imagePreviewUrl: item.brand_logo},
      brandName: item.brand_name
    });
  }

  handleBrandNameChange(event) {
    this.setState({
      brandNameChanged: true,
      brandName: event.target.value
    });
  }

  handleImageChange(event) {
    this.setState({imageSelected : true});
    event.preventDefault();

    let reader = new FileReader();
    let file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({
        image:{
          file: file,
          imagePreviewUrl: reader.result
        }
      });
    }
    reader.readAsDataURL(file);
    console.log(file);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
  }

  //End
  //Ajax Calls Start

  getBrandList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getBrandList',{
      method: 'GET',
      headers: {
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        this.setState({
          brandList :json.data
        });
     }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  handleToggle(item){
    var con;
    if(item.flag === 1){
      con = window.confirm('Want to DeActivate Brand?');
    }
    if(item.flag === 0){
      con = window.confirm('Want to Activate Brand?');
    }
    if(con){
      fetch('http://softbizz.in/helmetLaundry/api/public/toggleBrand',{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'session-key': sessionStorage.getItem("key"),
          'user-id' : sessionStorage.getItem("user")
        },
        body: JSON.stringify({
          'brand_id': item.brand_id,
        })
      })
      .then(response => response.json())
      .then(json =>{
        if(json.error === true){
          if(json.sessionExpired === true){
            window.location.hash = 'login';
          }
          alert(json.message);
        }
        else{
          alert(json.message);
          this.handleClose();
          this.getBrandList();
        }
      })
      .catch(error =>{
        console.log(error);
      });
    }
  }

  updateBrand(){
    if(this.state.brandName === ''){
      alert('Please Enter Brand Name');
      return;
    }
    if(this.state.image.imagePreviewUrl === ''){
      alert('Please Select Brand Image');
      return;
    }
    var brandName = this.state.brandName;
    var image = this.state.image.imagePreviewUrl;
    var tempBrandName = this.state.tempData.brand_name;
    var tempImage = this.state.tempData.brand_logo;
    if(brandName === tempBrandName && image === tempImage){
      alert('No Changes to Update');
      return;
    }
    var imagePreviewUrl;
    if(this.state.imageSelected === false){
      imagePreviewUrl = '';
    }
    else{
      imagePreviewUrl = this.state.image.imagePreviewUrl;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/updateBrand',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
      body: JSON.stringify({
        'brand_id': this.state.tempData.brand_id,
        'brand_name': this.state.brandName,
        'brand_logo': imagePreviewUrl
      })
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        alert(json.message);
        this.handleClose();
        this.getBrandList();
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  addNewBrand(){
    if(this.state.brandName === ''){
      alert('Please Enter Brand Name');
      return;
    }
    if(this.state.image.imagePreviewUrl === ''){
      alert('Please Select Brand Image');
      return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/addBrand',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
      body: JSON.stringify({
        'brand_name': this.state.brandName,
        'brand_logo': this.state.image.imagePreviewUrl
      })
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        alert(json.message);
        this.handleClose();
        this.getBrandList();
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  //Ajax Calls End
  //Views start

  addModel(){
    let {imagePreviewUrl} = this.state.image;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<Image src={imagePreviewUrl} thumbnail width='200px' height='200px'/>);
    } else {
      $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    }
    
    return(
      <Modal show={this.state.showAddModel} onHide={this.handleClose.bind(this)}>
      <Modal.Header closeButton>
        <Modal.Title>Add New Brand</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container-fluid ">
          <div className='row'>
            <div className='col-sm-4'>
              <label>Brand Name</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' onChange={this.handleBrandNameChange.bind(this)} placeholder='Enter Brand Name'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Brand Logo</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type="file" onChange={(event)=>this.handleImageChange(event)} />     
              <div className="imgPreview">
                {$imagePreview}
              </div>
            </div>          
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
            </div> 
            <div className='col-sm-8'>
              <Button bsStyle='success' onClick={this.addNewBrand.bind(this)}>ADD</Button>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.handleClose.bind(this)}>Close</Button>
      </Modal.Footer>
    </Modal>
    )
  }

  editModel(){
    let {imagePreviewUrl} = this.state.image;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<Image src={imagePreviewUrl} thumbnail width='200px' height='200px'/>);
    } else {
      $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    }
    
    return(
      <Modal show={this.state.show} onHide={this.handleClose.bind(this)}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Brand</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container-fluid ">
          <div className='row'>
            <div className='col-sm-4'>
              <label>Brand Name</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' value={this.state.brandName} onChange={this.handleBrandNameChange.bind(this)} placeholder='Enter Brand Name'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Brand Logo</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type="file" onChange={(event)=>this.handleImageChange(event)} />     
              <div className="imgPreview">
                {$imagePreview}
              </div>
            </div>          
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
            </div> 
            <div className='col-sm-8'>
              <Button bsStyle='success' onClick={this.updateBrand.bind(this)}>Update</Button>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.handleClose.bind(this)}>Close</Button>
      </Modal.Footer>
    </Modal>
    )
  }

  tableData(){
    return (
      <Table responsive striped bordered condensed hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>Brand Name</th>
              <th>Brand Logo</th>
              <th>Options</th>
            </tr>
          </thead>
          <tbody>
            {this.state.brandList.map(item => {
              if(this.state.selectedOption.length <= 0 || item.brand_id === this.state.selectedOption.value ){
                  return (
                    <tr key={item.brand_id}>
                      <td>{item.brand_id}</td>
                      <td>{item.brand_name}</td>
                      <td><Image src={item.brand_logo} thumbnail width='100px' height='100px'/></td>
                      <td>
                        <ButtonToolbar>
                          <Button bsStyle="primary" bsSize="small" onClick={this.handleShowEditModel.bind(this, item)}>Edit</Button>
                          {item.flag === 1 ? 
                                 (<Button bsStyle="danger" bsSize="small" onClick={this.handleToggle.bind(this, item)}>Deactivate</Button>) 
                                :(<Button bsStyle="success" bsSize="small" onClick={this.handleToggle.bind(this, item)}>Activate</Button>)
                          }
                        </ButtonToolbar>
                      </td>
                    </tr>
                )
              }
              else{
                return null;
              }
            })}
          </tbody>
        </Table>
    )
  }
  
  render() {
    const selectList = this.state.brandList.map(json =>{
        return { value: json.brand_id , label: json.brand_name}
    })
    return (
      <div className="content">
          <Card
                // title="BRANDS"
                // category="List Of Brands"
                content={
                  <div>
                    <div className='row'>
                      <div className='col-sm-6'>
                        <Button bsStyle="success" onClick={this.handleShowAddModel.bind(this)}>ADD NEW BRAND</Button>
                      </div>
                      <div className='col-sm-6'>
                      <Select placeholder='Search By Brand Name...'
                        value={this.state.selectedOption}
                        onChange={this.handleChange}
                        options={selectList}
                      />
                      </div>
                    </div>
                    <br />
                    {this.tableData()}
                    {this.editModel()}
                    {this.addModel()}
                  </div>
                }
          />
      </div>
    );
  }
}

export default Brands;

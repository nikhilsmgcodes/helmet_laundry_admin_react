import React, { Component } from "react";
import {Table, ButtonToolbar, Button, Modal } from "react-bootstrap";
import Card from "components/Card/Card";
import Select from 'react-select';

class Tickets extends Component {
    constructor(props){
        super(props);
    
        this.state = {
            ticketList: [],
            selectedOption: [],
            tempData: {},
            showViewModel :false,
            replyMessage: '',
        };
        if(!sessionStorage.getItem("key") || !sessionStorage.getItem("user")){
          window.location.hash = '/login';
        }
    }

    componentDidMount(){
        this.getTicketList();
     }

     handleChange = (selectedOption) => {
        this.setState({selectedOption});
    }

    handleShowViewModel(item){
        this.setState({
            tempData: item,
            showViewModel :true,
        })
    }

    handleClose(){
        this.setState({
          showViewModel: false,
          tempData: {},
          replyMessage: '',
        });
      }
      
    handleReplyChange(event){
        this.setState({
            replyMessage:event.target.value
        })
    }

    //Ajjax Calls Start
    getTicketList(){
        fetch('http://softbizz.in/helmetLaundry/api/public/getTickets',{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'session-key': sessionStorage.getItem("key"),
            'user-id' : sessionStorage.getItem("user"),
            },
            body: JSON.stringify({
                'user' : 'admin'
            })
        })
        .then(response => response.json())
        .then(json =>{
            if(json.error === true){
              if(json.sessionExpired === true){
                window.location.hash = 'login';
              }
            alert(json.message);
            }
            else{
            this.setState({
                ticketList :json.data
            });
            }
        })
        .catch(error =>{
            console.log(error);
        });
      }

      updateMessage(){
        fetch('http://softbizz.in/helmetLaundry/api/public/updateComment',{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'session-key': sessionStorage.getItem("key"),
              'user-id' : sessionStorage.getItem("user")
            },
            body: JSON.stringify({
              'comment': this.state.replyMessage,
              'trouble_ticket_id': this.state.tempData.trouble_ticket_id,
              'user': 'admin'
            })
          })
          .then(response => response.json())
          .then(json =>{
            if(json.error === true){
              if(json.sessionExpired === true){
                window.location.hash = 'login';
              }
              alert(json.message);
            }
            else{
              alert(json.message);
              this.handleClose();
              this.getTicketList();
            }
          })
          .catch(error =>{
            console.log(error);
          });
      }

      closeTicket(){
        var con = window.confirm('Want to close the Ticket?');
        if(con){
          fetch('http://softbizz.in/helmetLaundry/api/public/toggleTicket',{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'session-key': sessionStorage.getItem("key"),
              'user-id' : sessionStorage.getItem("user")
            },
            body: JSON.stringify({
              'trouble_ticket_id': this.state.tempData.trouble_ticket_id,
            })
          })
          .then(response => response.json())
          .then(json =>{
            if(json.error === true){
              if(json.sessionExpired === true){
                window.location.hash = 'login';
              }
              alert(json.message);
            }
            else{
              alert(json.message);
              this.handleClose();
              this.getTicketList();
            }
          })
          .catch(error =>{
            console.log(error);
          });
        } 
      }

      //Ajax Calls End
      //Views Start

      viewModel(){
        const spaceStyle = {
            whiteSpace:'pre-line',
        }

        var buttonData = [];
        if(this.state.tempData.flag === 1){
          buttonData.push(
              <div>
                <div className='row'>
                  <div className='col-sm-8'>
                      <textarea className="form-control" rows={1} placeholder='Reply Message...' onChange={this.handleReplyChange.bind(this)} />
                  </div> 
                  <div className='col-sm-4'>
                      <Button bsStyle="primary" bsSize="medium" block onClick={this.updateMessage.bind(this)}>Submit</Button>
                  </div>
                </div>
                <br></br>
                <div className='row'>
                  <div className='col-sm-12'>
                      <Button bsStyle="danger" bsSize="medium" block onClick={this.closeTicket.bind(this)}>Close Ticket</Button>
                  </div>
                </div>
              </div>
            )
        }
        
        if(this.state.showViewModel === true){
        return(
          <Modal show={this.state.showViewModel} onHide={this.handleClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Ticket Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="container-fluid ">
              <div className='row'>
                <div className='col-sm-6'>
                    <p><b>Ticket ID: </b>{this.state.tempData.trouble_ticket_id}</p>
                </div>
                <div className='col-sm-6'>
                    <p><b>Order ID: </b>{this.state.tempData.order_id}</p>
                </div>
              </div>
              <div className='row'>
                <div className='col-sm-6'>
                    <p><b>Open Date: </b>{this.state.tempData.ticket_open_date_time}</p>
                </div>
                <div className='col-sm-6'>
                    <p><b>Close Date: </b>{this.state.tempData.ticket_close_date_time}</p>
                </div>
              </div>
              <hr></hr>
              <div className='row'>
                <div className='col-sm-12'>
                  <p style={spaceStyle}>{this.state.tempData.comments}</p>
                </div>      
              </div>
              <br></br>
              {buttonData}
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.handleClose.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal>
        )
        }
      }

      tableData(){
        return (
          <Table responsive striped bordered condensed hover>
              <thead>
                <tr>
                  <th>Ticket ID</th>
                  <th>Order ID</th>
                  <th>Open Date & Time</th>
                  <th>Closed Date & Time</th>
                  <th>Status</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                {this.state.ticketList.map(item => {
                  if(this.state.selectedOption.length <= 0 || item.trouble_ticket_id === this.state.selectedOption.value){
                      return (
                        <tr key={item.trouble_ticket_id}>
                          <td>{item.trouble_ticket_id}</td>
                          <td>{item.order_id}</td>
                          <td>{item.ticket_open_date_time}</td>
                          <td>{item.ticket_close_date_time}</td>
                          <td>{item.flag === 1 ? 'OPEN' : 'CLOSED'}</td>
                          <td>
                            <ButtonToolbar>
                              <Button bsStyle="primary" bsSize="small" onClick={this.handleShowViewModel.bind(this, item)}>View</Button>
                            </ButtonToolbar>
                          </td>
                        </tr>
                    )
                  }
                  else{
                    return null;
                  }
                })}
              </tbody>
            </Table>
        )
      }

      render() {
        var selectList = this.state.ticketList.map(json =>{
            return { value: json.trouble_ticket_id , label: "Ticket ID: " + json.trouble_ticket_id + " | Order ID: " + json.order_id}
        });
        
        return (
            <div className="content">
                <Card
                        // title="BRANDS"
                        // category="List Of Brands"
                        content={
                        <div>
                            <div className='row'>
                            <div className='col-sm-6'>
                            <Select placeholder='Search Ticket...'
                                value={this.state.selectedOption}
                                onChange={this.handleChange}
                                options={selectList}
                            />
                            </div>
                            </div>
                            <br />
                            {this.tableData()}
                            {this.viewModel()}
                            {/* {this.addModel()} */}
                        </div>
                        }
                />
            </div>
        )
    }
}

export default Tickets;
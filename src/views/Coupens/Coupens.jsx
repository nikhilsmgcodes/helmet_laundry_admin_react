import React, { Component } from "react";
import {Table, ButtonToolbar, Button, Modal } from "react-bootstrap";
import Card from "components/Card/Card";
import Select from 'react-select';

class Coupens extends Component {
    constructor(props){
        super(props);
    
        this.state = {
          coupenList : [],
          selectedOption :[],
          showAddModel: false,
          coupenName: '',
          coupenDiscountPercentage: 0,
          showEditModel: false,
          tempData: {},
        };
        if(!sessionStorage.getItem("key") || !sessionStorage.getItem("user")){
          window.location.hash = '/login';
        }
    }

    componentDidMount(){
       this.getCoupenList();
    }

    handleChange = (selectedOption) => {
        this.setState({selectedOption});
    }

    handleShowAddModel(){
      this.setState({
        showAddModel: true,
      });
    }

    handleClose(){
      this.setState({
        showAddModel: false,
        coupenName: '',
        coupenDiscountPercentage: Number,
        showEditModel: false,
        tempData: {},
      });
    }

    handleCoupenNameChange(event){
      this.setState({
        coupenName: event.target.value
      });
    }

    handleDiscountChange(event){
      this.setState({
        coupenDiscountPercentage: event.target.value
      });
    }

    handleShowEditModel(item){
      this.setState({
        showEditModel:true,
        coupenName: item.coupen_name,
        coupenDiscountPercentage: item.coupon_discount_percentage,
        tempData: item,
      })
    }

    //Ajjax Calls Start
    getCoupenList(){
      fetch('http://softbizz.in/helmetLaundry/api/public/getCoupenList',{
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      }
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        this.setState({
          coupenList :json.data
        });
      }
    })
    .catch(error =>{
      console.log(error);
    });
    }

    addNewCoupen(){
      if(this.state.coupenName === ''){
        alert('Please Enter Coupen Name');
        return;
      }
      fetch('http://softbizz.in/helmetLaundry/api/public/addCoupen',{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'session-key': sessionStorage.getItem("key"),
          'user-id' : sessionStorage.getItem("user")
        },
        body: JSON.stringify({
          'coupen_name': this.state.coupenName,
          'coupon_discount_percentage': this.state.coupenDiscountPercentage
        })
      })
      .then(response => response.json())
      .then(json =>{
        if(json.error === true){
          if(json.sessionExpired === true){
            window.location.hash = 'login';
          }
          alert(json.message);
        }
        else{
          alert(json.message);
          this.handleClose();
          this.getCoupenList();
        }
      })
      .catch(error =>{
        console.log(error);
      });
    }

    updateCoupen(){
      var t =this.state;
      if(t.coupenName === ''){
        alert('Please Enter Coupen Name');
        return;
      }
      
      if(t.coupenName === t.tempData.coupen_name && t.coupenDiscountPercentage === t.tempData.coupon_discount_percentage){
        alert('No Changes to Update');
        return;
      }
      fetch('http://softbizz.in/helmetLaundry/api/public/updateCoupen',{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'session-key': sessionStorage.getItem("key"),
          'user-id' : sessionStorage.getItem("user")
        },
        body: JSON.stringify({
          'coupen_id': this.state.tempData.coupen_id,
          'coupen_name': this.state.coupenName,
          'coupon_discount_percentage': this.state.coupenDiscountPercentage
        })
      })
      .then(response => response.json())
      .then(json =>{
        if(json.error === true){
          if(json.sessionExpired === true){
            window.location.hash = 'login';
          }
          alert(json.message);
        }
        else{
          alert(json.message);
          this.handleClose();
          this.getCoupenList();
        }
      })
      .catch(error =>{
        console.log(error);
      });
    }

    handleToggle(item){
      var con;
      if(item.flag === 1){
        con = window.confirm('Want to DeActivate Coupen?');
      }
      if(item.flag === 0){
        con = window.confirm('Want to Activate Coupen?');
      }
      if(con){
        fetch('http://softbizz.in/helmetLaundry/api/public/toggleCoupen',{
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'session-key': sessionStorage.getItem("key"),
            'user-id' : sessionStorage.getItem("user")
          },
          body: JSON.stringify({
            'coupen_id': item.coupen_id,
          })
        })
        .then(response => response.json())
        .then(json =>{
          if(json.error === true){
            if(json.sessionExpired === true){
              window.location.hash = 'login';
            }
            alert(json.message);
          }
          else{
            alert(json.message);
            this.handleClose();
            this.getCoupenList();
          }
        })
        .catch(error =>{
          console.log(error);
        });
      }
    }

    //Ajax Call End
    //Views Start

    editModel(){
      if(this.state.showEditModel === true){
      return(
        <Modal show={this.state.showEditModel} onHide={this.handleClose.bind(this)}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Coupen</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container-fluid ">
            <div className='row'>
              <div className='col-sm-4'>
                <label>Coupen Code</label>
              </div>
              <div className='col-sm-8'>
                <input className="form-control" type='text' value={this.state.coupenName} onChange={this.handleCoupenNameChange.bind(this)} placeholder='Enter Coupen Name/Code'/>
              </div>
            </div>
            <br></br>
            <div className='row'>
              <div className='col-sm-4'>
                <label>Coupen Discount Percentage %</label>
              </div>
              <div className='col-sm-8'>
                <input className="form-control" type='number' value={this.state.coupenDiscountPercentage} onChange={this.handleDiscountChange.bind(this)} placeholder='Enter Coupen Discount Percentage' />
              </div>      
            </div>
            <br></br>
            <div className='row'>
              <div className='col-sm-4'>
              </div> 
              <div className='col-sm-8'>
                <Button bsStyle='success' onClick={this.updateCoupen.bind(this)}>Update</Button>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.handleClose.bind(this)}>Close</Button>
        </Modal.Footer>
      </Modal>
      )
      }
    }

    addModel(){
      if(this.state.showAddModel === true){
      return(
        <Modal show={this.state.showAddModel} onHide={this.handleClose.bind(this)}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Coupen</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container-fluid ">
            <div className='row'>
              <div className='col-sm-4'>
                <label>Coupen Code</label>
              </div>
              <div className='col-sm-8'>
                <input className="form-control" type='text' onChange={this.handleCoupenNameChange.bind(this)} placeholder='Enter Coupen Name/Code'/>
              </div>
            </div>
            <br></br>
            <div className='row'>
              <div className='col-sm-4'>
                <label>Coupen Discount Percentage %</label>
              </div>
              <div className='col-sm-8'>
                <input className="form-control" type='number' onChange={this.handleDiscountChange.bind(this)} placeholder='Enter Coupen Discount Percentage'/>
              </div>      
            </div>
            <br></br>
            <div className='row'>
              <div className='col-sm-4'>
              </div> 
              <div className='col-sm-8'>
                <Button bsStyle='success' onClick={this.addNewCoupen.bind(this)}>ADD</Button>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.handleClose.bind(this)}>Close</Button>
        </Modal.Footer>
      </Modal>
      )
      }
    }

    tableData(){
        return (
          <Table responsive striped bordered condensed hover>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Coupen Code</th>
                  <th>Discount Percentage %</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                {this.state.coupenList.map(item => {
                  if(this.state.selectedOption.length <= 0 || item.coupen_id === this.state.selectedOption.value){
                      return (
                        <tr key={item.coupen_id}>
                          <td>{item.coupen_id}</td>
                          <td>{item.coupen_name}</td>
                          <td>{item.coupon_discount_percentage}</td>
                          <td>
                            <ButtonToolbar>
                              <Button bsStyle="primary" bsSize="small" onClick={this.handleShowEditModel.bind(this, item)}>Edit</Button>
                              {item.flag === 1 ? 
                                 (<Button bsStyle="danger" bsSize="small" onClick={this.handleToggle.bind(this, item)}>Deactivate</Button>) 
                                :(<Button bsStyle="success" bsSize="small" onClick={this.handleToggle.bind(this, item)}>Activate</Button>)
                              }
                            </ButtonToolbar>
                          </td>
                        </tr>
                    )
                  }
                  else{
                    return null;
                  }
                })}
              </tbody>
            </Table>
        )
      }

    render() {
        var selectList = this.state.coupenList.map(json =>{
            return { value: json.coupen_id , label: json.coupen_id + ': ' + json.coupen_name}
        });
        
        return (
            <div className="content">
                <Card
                        // title="BRANDS"
                        // category="List Of Brands"
                        content={
                        <div>
                            <div className='row'>
                            <div className='col-sm-6'>
                                <Button bsStyle="success" onClick={this.handleShowAddModel.bind(this)}>ADD NEW COUPEN</Button>
                            </div>
                            <div className='col-sm-6'>
                            <Select placeholder='Search Coupen...'
                                value={this.state.selectedOption}
                                onChange={this.handleChange}
                                options={selectList}
                            />
                            </div>
                            </div>
                            <br />
                            {this.tableData()}
                            {this.editModel()}
                            {this.addModel()}
                        </div>
                        }
                />
            </div>
        )
    }
}

export default Coupens;
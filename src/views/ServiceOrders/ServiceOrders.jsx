import React, { Component } from "react";
import {Table, ButtonToolbar, Button, Modal } from "react-bootstrap";
import Card from "components/Card/Card";
import Select from 'react-select';

class ServiceOrders extends Component {

  constructor(props){
    super(props);

    this.state = {
      orderList : [],
      categoryList : [],
      statusList: [],
      showViewModel: false,
      selectedOrderId: [],
      selectedStatus: [],
      tempData: {categoryOrders:[{service_orders:[{service_name:''}]}]},
      tempDataOriginal: {},
      updateNewStatus: [],
    };
    if(!sessionStorage.getItem("key") || !sessionStorage.getItem("user")){
      window.location.hash = '/login';
    }
  }

  componentDidMount(){
    this.getOrdersByStatus(1);
    this.getStatusList();
  }

  handleClose() {
    this.setState({
      showViewModel: false,
      tempData: {categoryOrders:[{service_orders:[]}]},
      tempDataOriginal: {},
    });
  }

  handleShowViewModel(item) {
    this.setState({
      showViewModel: true,
      tempData: item,
      tempDataOriginal: item,
      updateNewStatus: {value: item.status_id , label: item.status},
    });
  }

  handleStatusChange = (selectedStatus) => {
    this.setState({ selectedStatus });
    if(selectedStatus.length !== 0){
      this.getOrdersByStatus(selectedStatus.value);
    }
  }

  handleOrderIdChange = (selectedOrderId) => {
    this.setState({ selectedOrderId });
  }
  
  handleNewStatusChange = (updateNewStatus) => {
    this.setState({updateNewStatus});
  }
  

  //Ajax Call Start

  getOrdersByStatus(status){
    this.setState({orderList: []});
    fetch('http://softbizz.in/helmetLaundry/api/public/getOrdersByStatus',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
      body: JSON.stringify({
        // 'order_type' : 0,
        'status_id' : status,
      })
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        this.setState({
          orderList :json.data
        });
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  getStatusList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getStatusList',{
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      }
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        this.setState({
          statusList :json.data
        });
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  updateOrderStatus(status){
    if(this.state.tempData.order_status_id === status.value){
      alert('No Changes to Update');
      return;
    }
    if(status.value === ''){
      alert('Please select the status to Update');
      return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/updateOrderStatus',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
      body: JSON.stringify({
        // 'order_type' : 0,
        'order_status_id' : status.value,
        'order_id' : this.state.tempData.order_id,
      })
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
          alert(json.message);
          this.getOrdersByStatus(this.state.tempData.order_status_id);
          this.handleClose();
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  //Ajax Call End
  //Views Start

  editModel(){
    const statusList = this.state.statusList.map(json =>{
        return json.status_id !== '1' ? { value: json.status_id , label: json.status} : {};
    });

    var t = this.state.tempData;
    var tData = [];

    for(var i=0; i<t.categoryOrders.length; i++){
      tData.push(
        <tr key={i}>
          <td>
              <b>Category: </b>{t.categoryOrders[i].category_name} <br/>
              <b>Services: </b>{t.categoryOrders[i].service_orders.map(item => {
                  return (<div key={item.service_id}> {item.service_name} <br/></div>)
              })}
          </td>
          <td><br/><br/>{t.categoryOrders[i].service_orders.map(item => {
                  return (<div> {item.price} <br/></div>)
              })}
          </td>
        </tr>
      )
    }

    tData.push(
      <tr>
        <td><b>total:</b></td>
        <td>{t.total}</td>
      </tr>
    )
    
    if(t.coupen_discount && t.coupen_discount!==0){
      tData.push(
        <tr>
          <td><b>Coupen Discount:</b></td>
          <td>{t.coupen_discount}</td>
        </tr>
      )
    }

    tData.push(
      <tr>
      <td><b>Pickup Delivery: </b>{t.pickup_delivery_name}</td>
      <td>{t.pickup_delivery_charge}</td>
    </tr>
    )

    tData.push(
      <tr>
      <td><b>GST:</b></td>
      <td>{t.gst}</td>
    </tr>
    )

    tData.push(
      <tr>
      <td><b>Grand Total:</b></td>
      <td>{t.grand_total}</td>
    </tr>
    )

    // for(var j=0; j<t.categoryOrders.length; j++){
    //   for(var k=0; j<t.categoryOrders[j].service_orders.length; k++){
    //     servicesData.push(
    //       <div>
    //         {t.categoryOrders[j].service_orders[k].service_name}
    //       </div>
    //     )
    //   }
    // }


    // var totalPrice = 0, totalGst=0, totalAmount=0, discountAmount =0, grandTotal =0;
    // if(t.services.length !== 0){
    //   for(var i=0; i<t.services.length; i++){
    //     tData.push(
    //       <tr key={t.services[i].service_id}>
    //         <td>{t.services[i].service_name}</td>
    //         <td>{t.services[i].price}</td>
    //       </tr>
    //     )
    //     totalPrice += parseInt(t.services[i].price, 10);
    //     totalGst = totalPrice * 18/100;
    //     totalAmount += parseInt(t.services[i].price, 10) + (parseInt(t.services[i].price, 10) * 18/100);
    //     grandTotal = totalAmount + totalGst + parseInt(t.pickup_delivery_charge, 10);
    //   }

    //   tData.push(
    //     <tr key = 'total'>
    //       <td><b>Total</b></td>
    //       <td><b>{totalPrice}</b></td>
    //     </tr>
    //   )

    //   tData.push(
    //     <tr key = 'gst'>
    //       <td><b>GST 18%</b></td>
    //       <td>+ {totalGst}</td>
    //     </tr>
    //   )

    //   tData.push(
    //     <tr key = 'pickup/delivery'>
    //       <td><b>Pickup/Delivery: </b> {t.pickup_delivery_name}</td>
    //       <td>+ {t.pickup_delivery_charge}</td>
    //     </tr>
    //   )

    //   if(t.coupenData.length !==0){
    //     discountAmount = totalPrice - (totalPrice * t.coupenData[0].coupon_discount_percentage/100);
    //     grandTotal -= discountAmount;
    //     tData.push(
    //       <tr key='coupen'>
    //         <td><b>Coupen Discount: {t.coupenData[0].coupen_name}</b></td>
    //         <td>- {discountAmount}</td>
    //       </tr>
    //     )
    //   }

    //   tData.push(
    //     <tr key='grandTotal'>
    //       <td><b>Grand Total</b></td>
    //       <td><b>{grandTotal}</b></td>
    //     </tr>
    //   )
    // }

    var statusUpdateButton = [];

    if(t.status_id !== '1'){
      statusUpdateButton.push(
          <div className='row' key='statusUpdate'>
            <div className='col-sm-3'>
              <label>Change Order Status: </label>
            </div>
            <div className='col-sm-6'>
              <Select placeholder='Select Order Status..'
                            value={this.state.updateNewStatus}
                            onChange={this.handleNewStatusChange}
                            options={statusList}
                          />
            </div>
            <div className='col-sm-3'>
              <Button show='false' bsStyle="primary" bsSize="small" onClick={this.updateOrderStatus.bind(this, this.state.updateNewStatus)}>Update</Button>
            </div>
          </div>
      )
    }

    return(
      <Modal show={this.state.showViewModel} onHide={this.handleClose.bind(this)} bsSize="lg">
      <Modal.Header closeButton>
        <Modal.Title>Order Details</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container-fluid ">
          <div className='row'>
            <div className='col-sm-3'>
              <label>Order ID: </label> {t.order_id}
            </div>
            {/* <div className='col-sm-3'>
              <label>Category: </label> {t.category_name}
            </div>
            <div className='col-sm-3'>
              <label>Brand: </label> {t.brand_name}
            </div> */}
            <div className='col-sm-3'>
              <label>Status: </label> {t.status}
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label><u>CUSTOMER DETAILS</u></label><br></br>
              <label>ID:</label> {t.user_id}<br></br>
              <label>Name:</label> {t.name}<br></br>
              <label>Phone:</label> {t.phone}<br></br>
              <label>Email:</label> {t.email}
            </div>
            <div className='col-sm-4'>
              <label><u>PICKUP DETAILS</u></label><br></br>
              <label>Date:</label> {t.pick_up_date}<br></br>
              <label>Time:</label> {t.pick_up_time}<br></br>
              <label>Address:</label> {t.pick_up_address}
            </div>
            <div className='col-sm-4'>
              <label><u>DELIVERY DETAILS</u></label><br></br>
              <label>Date:</label> {t.delivery_date}<br></br>
              <label>Time:</label> {t.delivery_time}<br></br>
              <label>Address:</label> {t.delivery_address}
            </div>
          </div>
          <Table responsive striped bordered condensed hover>
            <thead>
              <tr>
                <th><b>Service</b></th>
                <th><b>Price</b></th>
              </tr>
            </thead>
            <tbody>
              {tData}
            </tbody>
          </Table>
          <br></br>
            {statusUpdateButton}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.handleClose.bind(this)}>Close</Button>
      </Modal.Footer>
    </Modal>
    )
  }

  tableData(){
    return (
      <Table responsive striped bordered condensed hover>
          <thead>
            <tr>
              <th>Order ID</th>
              <th>User Name</th>
              <th>Pickup/Delivery Name</th>
              <th>Pickup Date & Time</th>
              <th>Delivery Date & Time</th>
              <th>Status</th>
              <th>Options</th>
            </tr>
          </thead>
          <tbody>
            {this.state.orderList.map(item => {
              if(this.state.selectedOrderId.length <= 0 || item.order_id === this.state.selectedOrderId.value){
                  return (
                    <tr key={item.order_id}>
                      <td>{item.order_id}</td>
                      <td>{item.name}</td>
                      <td>{item.pickup_delivery_name}</td>
                      <td>{item.pick_up_date} , {item.pick_up_time}</td>
                      <td>{item.delivery_date} , {item.delivery_time}</td>
                      <td>{item.status}</td>
                      <td>
                        <ButtonToolbar>
                          <Button bsStyle="primary" bsSize="small" onClick={this.handleShowViewModel.bind(this, item)}>View</Button>
                        </ButtonToolbar>
                      </td>
                    </tr>
                )
              }
              else{
                return null;
              }
            })}
          </tbody>
        </Table>
    )
  }

  render() {
    const orderIdList = this.state.orderList.map(json =>{
        return { value: json.order_id , label: json.order_id}
    });

    const statusList = this.state.statusList.map(json =>{
      return { value: json.status_id , label: json.status}
    });

    return (
      <div className="content">
          <Card
                // title="BRANDS"
                // category="List Of Categorys"
                content={
                  <div>
                    <div className='row'>
                      <div className='col-sm-4'>
                        <Select placeholder='Get Orders List By Status..'
                          value={this.state.selectedStatus}
                          onChange={this.handleStatusChange}
                          options={statusList}
                        />
                      </div>
                      <div className='col-sm-4'>
                        <Select placeholder='Search By Order ID...'
                          value={this.state.selectedOrderId}
                          onChange={this.handleOrderIdChange}
                          options={orderIdList}
                        />
                      </div>
                    </div>
                    <br />
                    {this.tableData()}
                    {this.editModel()}
                  </div>
                }
          />
      </div>
    );
  }
}

export default ServiceOrders;

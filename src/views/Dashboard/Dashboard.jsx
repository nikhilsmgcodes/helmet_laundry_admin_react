import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
// import { Link,Route, Switch, Redirect } from "react-router-dom";
// import { Card } from "components/Card/Card.jsx";
import { StatsCard } from "components/StatsCard/StatsCard.jsx";

class Dashboard extends Component {
  constructor(props){
    super(props);

    this.state = {
      dashboardList : [],
    };
    if(!sessionStorage.getItem("key") || !sessionStorage.getItem("user")){
      window.location.hash = '/login';
    }
  }

  componentDidMount(){
    this.getDashboardInfo();
  }

  getDashboardInfo(){
    fetch('http://softbizz.in/helmetLaundry/api/public/adminDashboardInfo',{
      method: 'GET',
      headers: {
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        this.setState({
          dashboardList :json.data
        });
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  render() {
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-keypad text-warning" />}
                statsText="Category"
                statsValue={this.state.dashboardList.category}
                // statsIcon={<i className="fa fa-refresh" />}
                // statsIconText="Updated now"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-tools text-success" />}
                statsText="Subcategory"
                statsValue={this.state.dashboardList.subcategory}
                // statsIcon={<i className="fa fa-calendar-o" />}
                // statsIconText="Last day"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-ticket text-danger" />}
                statsText="Services"
                statsValue={this.state.dashboardList.services}
                // statsIcon={<i className="fa fa-clock-o" />}
                // statsIconText="In the last hour"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-way text-info" />}
                statsText="Pickup/Delivery"
                statsValue={this.state.dashboardList.pickup_delivery_services}
                // statsIcon={<i className="fa fa-refresh" />}
                // statsIconText="Updated now"
              />
            </Col>
          </Row>
          <Row>
             <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-users text-warning" />}
                statsText="Users"
                statsValue={this.state.dashboardList.users}
                // statsIcon={<i className="fa fa-refresh" />}
                // statsIconText="Updated now"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-scissors text-success" />}
                statsText="Coupens"
                statsValue={this.state.dashboardList.coupens}
                // statsIcon={<i className="fa fa-refresh" />}
                // statsIconText="Updated now"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-headphones text-danger" />}
                statsText="Tickets"
                statsValue={this.state.dashboardList.tickets}
                // statsIcon={<i className="fa fa-clock-o" />}
                // statsIconText="In the last hour"
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Dashboard;

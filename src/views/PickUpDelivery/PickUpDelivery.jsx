import React, { Component } from "react";
import {Table, ButtonToolbar, Button, Modal } from "react-bootstrap";
import Card from "components/Card/Card";
import Select from 'react-select';

class PickUpDelivery extends Component {

  constructor(props){
    super(props);

    this.state = {
      pickupDeliveryList : [],
      show: false,
      showAddModel: false,
      tempData: {pickup_delivery_name:'', pickup_delivery_charge:''},
      tempData1: {pickup_delivery_name:'', pickup_delivery_charge:''},
      selectedOption: []
    };
    if(!sessionStorage.getItem("key") || !sessionStorage.getItem("user")){
      window.location.hash = '/login';
    }
  }

  componentDidMount(){
    this.getPickupDeliveryServicesList();
  }

  handleClose() {
    this.setState({
       show: false, 
       showAddModel: false,
       tempData: {pickup_delivery_name:'', pickup_delivery_charge:''},
       tempData1: {pickup_delivery_name:'', pickup_delivery_charge:''}
      });
  }

  handleShowAddModel(){
    this.setState({ showAddModel: true });
  }

  handleShowEditModel(item) {
    this.setState({ 
      show: true,
      tempData: item,
      tempData1 : item,
    });
  }

  handleNameChange(event) {
    this.setState({
      tempData:{...this.state.tempData, pickup_delivery_name: event.target.value}
    });
  }

  handleChargeChange(event) {
    this.setState({
        tempData:{...this.state.tempData, pickup_delivery_charge: event.target.value}
      });
  }

  handleSelectChange = (selectedOption) => {
    this.setState({ selectedOption });
  }

  getPickupDeliveryServicesList(){
    fetch('http://softbizz.in/helmetLaundry/api/public/getPickupDeliveryServicesList',{
      method: 'GET',
      headers: {
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
      this.setState({
        pickupDeliveryList :json.data
      });
    }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  handleToggle(item){
    var con;
    if(item.flag === 1){
      con = window.confirm('Want to DeActivate Pickup/Delivery Service?');
    }
    if(item.flag === 0){
      con = window.confirm('Want to Activate Pickup/Delivery Service?');
    }
    if(con){
      fetch('http://softbizz.in/helmetLaundry/api/public/togglePickupDeliveryService',{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'session-key': sessionStorage.getItem("key"),
          'user-id' : sessionStorage.getItem("user")
        },
        body: JSON.stringify({
          'pickup_delivery_id': item.pickup_delivery_id
        })
      })
      .then(response => response.json())
      .then(json =>{
        if(json.error === true){
          if(json.sessionExpired === true){
            window.location.hash = 'login';
          }
          alert(json.message);
        }
        else{
          alert(json.message);
          this.handleClose();
          this.getPickupDeliveryServicesList();
        }
      })
      .catch(error =>{
        console.log(error);
      });
    }
  }

  addPickupDeliveryService(){
    if(this.state.tempData.pickup_delivery_name === ''){
        alert('Please Enter Pickup/Delivery Name');
        return;
    }
    if(this.state.tempData.pickup_delivery_charge === ''){
        alert('Please Enter Pickup/Delivery Charge');
        return;
    }
    fetch('http://softbizz.in/helmetLaundry/api/public/addPickupDeliveryService',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
      body: JSON.stringify({
        'pickup_delivery_name': this.state.tempData.pickup_delivery_name,
        'pickup_delivery_charge': this.state.tempData.pickup_delivery_charge
      })
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        alert(json.message);
        this.handleClose();
        this.getPickupDeliveryServicesList();
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  updatePickupDelivery(){
    if(this.state.tempData.pickup_delivery_name === ''){
      alert('Please Enter Pickup/Delivery Name');
      return;
    }
    if(this.state.tempData.pickup_delivery_charge === ''){
        alert('Please Enter Pickup/Delivery Charge');
        return;
    }
    
    if(this.state.tempData === this.state.tempData1){
      alert('No Changes to Update');
      return;
    }

    fetch('http://softbizz.in/helmetLaundry/api/public/updatePickupDeliveryService',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      },
      body: JSON.stringify({
        'pickup_delivery_id': this.state.tempData.pickup_delivery_id,
        'pickup_delivery_name': this.state.tempData.pickup_delivery_name,
        'pickup_delivery_charge': this.state.tempData.pickup_delivery_charge
      })
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          window.location.hash = 'login';
        }
        alert(json.message);
      }
      else{
        alert(json.message);
        this.handleClose();
        this.getPickupDeliveryServicesList();
      }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  addModel(){
    return(
        <Modal show={this.state.showAddModel} onHide={this.handleClose.bind(this)}>
        <Modal.Header closeButton>
          <Modal.Title>Add Pickup/Delivery Service</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container-fluid ">
            <div className='row'>
              <div className='col-sm-4'>
                <label>Name</label>
              </div>
              <div className='col-sm-8'>
                <input className="form-control" type='text' onChange={this.handleNameChange.bind(this)} placeholder='Enter Pickup/Delivery Name'/>
              </div>
            </div>
            <br></br>
            <div className='row'>
              <div className='col-sm-4'>
                <label>Charge</label>
              </div>
              <div className='col-sm-8'>
                <input className="form-control" type='text' onChange={this.handleChargeChange.bind(this)} placeholder='Enter Pickup/Delivery Charge'/>
              </div>
            </div>
            <br></br>
            <div className='row'>
              <div className='col-sm-4'>
              </div> 
              <div className='col-sm-8'>
                <Button bsStyle='success' onClick={this.addPickupDeliveryService.bind(this)}>Add</Button>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.handleClose.bind(this)}>Close</Button>
        </Modal.Footer>
      </Modal>
      )
  }

  editModel(){
    return(
      <Modal show={this.state.show} onHide={this.handleClose.bind(this)}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Pickup/Delivery Service</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container-fluid ">
          <div className='row'>
            <div className='col-sm-4'>
              <label>Name</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' value={this.state.tempData.pickup_delivery_name} onChange={this.handleNameChange.bind(this)} placeholder='Enter Pickup/Delivery Name'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
              <label>Charge</label>
            </div>
            <div className='col-sm-8'>
              <input className="form-control" type='text' value={this.state.tempData.pickup_delivery_charge} onChange={this.handleChargeChange.bind(this)} placeholder='Enter Pickup/Delivery Charge'/>
            </div>
          </div>
          <br></br>
          <div className='row'>
            <div className='col-sm-4'>
            </div> 
            <div className='col-sm-8'>
              <Button bsStyle='success' onClick={this.updatePickupDelivery.bind(this)}>Update</Button>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.handleClose.bind(this)}>Close</Button>
      </Modal.Footer>
    </Modal>
    )
  }

  tableData(){
    return (
      <Table responsive striped bordered condensed hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>Pickup/Delivery Name</th>
              <th>Charge</th>
              <th>Options</th>
            </tr>
          </thead>
          <tbody>
            {this.state.pickupDeliveryList.map(item => {
              if(this.state.selectedOption.length <= 0 || item.pickup_delivery_id === this.state.selectedOption.value ){
                  return (
                    <tr key={item.pickup_delivery_id}>
                      <td>{item.pickup_delivery_id}</td>
                      <td>{item.pickup_delivery_name}</td>
                      <td>{item.pickup_delivery_charge}</td>
                      <td>
                        <ButtonToolbar>
                          <Button bsStyle="primary" bsSize="small" onClick={this.handleShowEditModel.bind(this, item)}>Edit</Button>
                          {item.flag === 1 ? 
                                 (<Button bsStyle="danger" bsSize="small" onClick={this.handleToggle.bind(this, item)}>Deactivate</Button>) 
                                :(<Button bsStyle="success" bsSize="small" onClick={this.handleToggle.bind(this, item)}>Activate</Button>)
                          }
                        </ButtonToolbar>
                      </td>
                    </tr>
                )
              }
              else{
                return null;
              }
            })}
          </tbody>
        </Table>
    )
  }
  
  render() {
    const selectList = this.state.pickupDeliveryList.map(json =>{
        return { value: json.pickup_delivery_id , label: json.pickup_delivery_name}
    })
    return (
      <div className="content">
          <Card
                // title="BRANDS"
                // category="List Of Brands"
                content={
                  <div>
                    <div className='row'>
                      <div className='col-sm-6'>
                        <Button bsStyle="success" onClick={this.handleShowAddModel.bind(this)}>Add Pickup/Delivery Option</Button>
                      </div>
                      <div className='col-sm-6'>
                      <Select placeholder='Search...'
                        value={this.state.selectedOption}
                        onChange={this.handleSelectChange}
                        options={selectList}
                      />
                      </div>
                    </div>
                    <br />
                    {this.tableData()}
                    {this.editModel()}
                    {this.addModel()}
                  </div>
                }
          />
      </div>
    );
  }
}

export default PickUpDelivery;

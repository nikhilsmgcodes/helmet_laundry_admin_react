import React, { Component } from "react";
import { NavItem, Nav } from "react-bootstrap";

class HeaderLinks extends Component {

  logout(){
    var con = window.confirm('Want to Logout?');
    if(con){
      sessionStorage.clear();
      window.location.hash = 'login';
    }
  }

  render() {
    return (
      <div>
        <Nav pullRight>
          <NavItem eventKey={3} onClick={this.logout.bind(this)}>
            Log out
          </NavItem>
        </Nav>
      </div>
    );
  }
}

export default HeaderLinks;

import Dashboard from "views/Dashboard/Dashboard";
// import UserProfile from "views/UserProfile/UserProfile";
//import TableList from "views/TableList/TableList";
//import Typography from "views/Typography/Typography";
// import Icons from "views/Icons/Icons";
//import Maps from "views/Maps/Maps";
// import Notifications from "views/Notifications/Notifications";
//import Upgrade from "views/Upgrade/Upgrade";

// import Brands from "views/Brands/Brands";
import Category from "views/Category/Category";
import Services from "views/Services/Services";
import PickUpDelivery from "views/PickUpDelivery/PickUpDelivery";
import Users from "views/Users/Users";
import ServiceOrders from "views/ServiceOrders/ServiceOrders";
import Coupens from "views/Coupens/Coupens";
import Tickets from "views/Tickets/Tickets";
import Login from "views/Login/Login";
import Subcategory from "views/Subcategory/Subcategory";


const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard
  },
  {
    path: "/category",
    name: "Category",
    icon: "pe-7s-keypad",
    component: Category
  },
  {
    path: "/subcategory",
    name: "Subcategory",
    icon: "pe-7s-keypad",
    component: Subcategory
  },
  {
    path: "/services",
    name: "Services",
    icon: "pe-7s-tools",
    component: Services
  },
  // {
  //   path: "/brands",
  //   name: "Brands",
  //   icon: "pe-7s-ticket",
  //   component: Brands
  // },
  {
    path: "/pickupDelivery",
    name: "Pickup & Delivery",
    icon: "pe-7s-way",
    component: PickUpDelivery
  },
  {
    path: "/users",
    name: "Users",
    icon: "pe-7s-users",
    component: Users
  },
  {
    path: "/coupens",
    name: "Coupens",
    icon: "pe-7s-scissors",
    component: Coupens
  },
  {
    path: "/serviceOrders",
    name: "Service Orders",
    icon: "pe-7s-cart",
    component: ServiceOrders
  },
  {
    path: "/tickets",
    name: "Tickets",
    icon: "pe-7s-headphones",
    component: Tickets
  },
  {
    path: "/login",
    name: "Login",
    icon: "pe-7s-headphones",
    component: Login
  },
  // {
  //   path: "/user",
  //   name: "User Profile",
  //   icon: "pe-7s-user",
  //   component: UserProfile
  // },
  // {
  //   path: "/table",
  //   name: "Table List",
  //   icon: "pe-7s-note2",
  //   component: TableList
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   icon: "pe-7s-news-paper",
  //   component: Typography
  // },
  // { path: "/icons", name: "Icons", icon: "pe-7s-science", component: Icons },
  // //{ path: "/maps", name: "Maps", icon: "pe-7s-map-marker", component: Maps },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   icon: "pe-7s-bell",
  //   component: Notifications
  // },
  // {
  //   upgrade: true,
  //   path: "/upgrade",
  //   name: "Upgrade to PRO",
  //   icon: "pe-7s-rocket",
  //   component: Upgrade
  // },
  { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
];

export default dashboardRoutes;

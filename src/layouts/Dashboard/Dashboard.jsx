import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import Header from "components/Header/Header";
import Footer from "components/Footer/Footer";
import Sidebar from "components/Sidebar/Sidebar";

import dashboardRoutes from "routes/dashboard.jsx";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      showLogin: false,
    };
  }

  checkSession(){
    fetch('http://softbizz.in/helmetLaundry/api/public/checkSessionData',{
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'session-key': sessionStorage.getItem("key"),
        'user-id' : sessionStorage.getItem("user")
      }
    })
    .then(response => response.json())
    .then(json =>{
      if(json.error === true){
        if(json.sessionExpired === true){
          this.setState({
            showLogin : true,
          })
        }
      }
      else{
        window.location.hash = '/dashboard';
        this.setState({
          showLogin : false,
      })
     }
    })
    .catch(error =>{
      console.log(error);
    });
  }

  componentDidMount() {
    if(!sessionStorage.getItem("key")){
      this.setState({
        showLogin : true,
      })
      window.location.hash = '/login';
      return;
    }
    else{
      this.checkSession();
    }
  }

  componentDidUpdate(e) {
    if(e.history.location.pathname === '/login'){
      if(this.state.showLogin === false){
        this.checkSession();
      }
    }
    if(e.history.location.pathname === '/dashboard'){
      if(this.state.showLogin === true){
        this.checkSession();
      }
    }
    if (
      window.innerWidth < 993 &&
      e.history.location.pathname !== e.location.pathname &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
    }
    if (e.history.action === "PUSH") {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.refs.mainPanel.scrollTop = 0;
    }
  }

  render() {
      if(this.state.showLogin === true){
        return(
          <Switch>
              {dashboardRoutes.map((prop, key) => {
                if (prop.redirect)
                  return <Redirect from={prop.path} to={prop.to} key={key} />;
                return (
                  <Route path={prop.path} component={prop.component} key={key} />
                );
              })}
          </Switch>
        )
      }
      else{
        return (
          <div className="wrapper">
            <Sidebar {...this.props} />
            <div id="main-panel" className="main-panel" ref="mainPanel">
              <Header {...this.props} />
              <Switch>
                {dashboardRoutes.map((prop, key) => {
                  if (prop.redirect)
                    return <Redirect from={prop.path} to={prop.to} key={key} />;
                  return (
                    <Route path={prop.path} component={prop.component} key={key} />
                  );
                })}
              </Switch>
              <Footer />
            </div>
          </div>
        );
      }
    }
  }

export default Dashboard;
